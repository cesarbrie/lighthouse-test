(function($)
{
    const $form = $('#survey')
    const $submitButton = $('[type="submit"]')

    const $messages = $('.messages')
    const $message = $('.message')

    const $close = $('.messages .dialog .close')

    $submitButton.on('click', e =>
    {
        e.preventDefault()

        const data = $form.serializeArray()
        data.push({name: 'ajax', value: 1})

        $.ajax(
        {
            url: window.location.pathname,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: res =>
            {
                $message.text(res.message)
                $messages.removeClass('hidden')
            },
        })
    })

    $close.on('click', () => $messages.addClass('hidden'))
}
)(jQuery);