<?php

    $ajax = $_POST['ajax'] == '1';
    $success = false;

    if ($ajax) {
        $name =  $_POST['name'];
        $goal1 = $_POST['goal-1'];
        $goal2 = $_POST['goal-2'];
        $goal3 = $_POST['goal-3'];

        if (!empty($_POST['name']) &&
        	!empty($_POST['goal-1']) &&
        	!empty($_POST['goal-2']) &&
        	!empty($_POST['goal-3'])) {

            $entry =
                "$name\r\n" .
                "1 | $goal1\r\n" .
                "2 | $goal2\r\n" .
                "3 | $goal3\r\n";

            file_put_contents('data.log', $entry . PHP_EOL, FILE_APPEND);

            $success = true;
        }

        $response = array
        (
            'message' => $success ?
                'Thanks for your answers' :
                'Please fill in all the fields'
        );

        exit(json_encode($response));
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Life goals survey</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <article>
        <form id="survey">
            <fieldset>
                <label>
                    <span>Your name: </span>
                    <input type="text" name="name"
                        placeholder="Please leave your name here"
                        <?php if ($ajax && !$success) echo "value=\"$name\"";?>
                    >
                </label>
            </fieldset>
            <fieldset>
                <h2>What are your 3 major life goals?</h2>
                <label>
                    <span>Goal #1: </span>
                    <input type="text" name="goal-1"
                        placeholder="Put your goal #1 here"
                        <?php if ($ajax && !$success)
                            echo "value=\"$goal1\"";?>
                    >
                </label>
                <label>
                    <span>Goal #2: </span>
                    <input type="text" name="goal-2"
                        placeholder="Put your goal #2 here"
                        <?php if ($ajax && !$success)
                            echo "value=\"$goal2\"";?>
                    >
                </label>
                <label>
                    <span>Goal #3: </span>
                    <input type="text" name="goal-3"
                        placeholder="Put your goal #3 here"
                        <?php if ($ajax && !$success)
                            echo "value=\"$goal3\"";?>
                    >
                </label>
            </fieldset>
            <input type="submit">
            <div class="messages hidden">
                <div class="dialog">
                    <p class="message"></p>
                    <button type="button" class="close"></button>
                </div>
            </div>
        </form>
    </article>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="scripts.js"></script>
    <!--
        Fool of me! This "test" is nothing more than a data-mining operation
        to have unsuspecting webdevs give out our life goals so we can be
        targeted with more relevant ads!
    -->
</body>
</html>